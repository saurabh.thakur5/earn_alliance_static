import React from "react";
import Button from "../../components/Button";
import { Container } from "../../Styles/LandingPage";

const LandingPage = () => {
  return (
    <Container>
      <Button />
    </Container>
  );
};
export default LandingPage;
