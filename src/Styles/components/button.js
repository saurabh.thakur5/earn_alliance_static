import styled from "styled-components";

export const ButtonWrapper = styled.div`
  position: relative;
`;
export const Btn = styled.button`
display: flex;
  background: rgba(238, 213, 165, 0.3);
  border: none;
  min-width: 269px;
  height: 53px;
  color: #fff;
  font-family: "Rubik", sans-serif;
  font-size: 18px;
  align-items: center;
  text-transform: capitalize;
  justify-content: center;
  clip-path: polygon(100% 0, 100% 100%, 24px 100%, 0% 30px, 0 0);
  span {
    padding: 10px;
    z-index: 2;
  }
  :hover {
    background: rgba(238, 213, 165, 1);
  }
`;
export const VectorWrap = styled.div`
  position: absolute;
  filter: drop-shadow(0 0 7px white);
  top: 37px;
  left: 1px;
`;
export const Vector = styled.div`
  clip-path: polygon(0 0, 0 100%, 100% 100%);
  box-shadow: 0 0 10px #fff;
  background: #fff;
  width: 15px;
  height: 15px;
`;
