import styled from "styled-components";
import backImage from "../assets/image111.png";

export const Container = styled.div`
  background: linear-gradient(180deg, rgba(0, 0, 0, 0) 0%, #000000 100%);
  background-image: url(${backImage});
  background-size: 120%;
  background-repeat: no-repeat;
  height: 100vh;
  display: grid;
  justify-content: center;
  align-items: center;
`;
