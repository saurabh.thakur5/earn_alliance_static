import React from "react";
import DiscordIcon from "../../assets/svgS/DiscordIcon";
import {
  Btn,
  ButtonWrapper,
  Vector,
  VectorWrap,
} from "../../Styles/components/button";

const Button = () => {
  return (
    <>
      <ButtonWrapper>
        <Btn>
          <DiscordIcon /> <span>join our discord</span>
        </Btn>
        <VectorWrap>
          <Vector />
        </VectorWrap>
      </ButtonWrapper>
    </>
  );
};

export default Button;
